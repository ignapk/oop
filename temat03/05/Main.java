import java.io.FileWriter;
import java.util.Arrays;
import java.io.IOException;
import java.util.Locale;
//
//
//
//class Ellipse implements Shape{
//    Vec2 center;
//    double rx,ry;
//
//    public Ellipse(Vec2 center, double rx, double ry) {
//        this.rx=rx;
//        this.ry=ry;
//        this.center=center;
//    }
//
//    @Override
//    public String toSvg() {
//        return String.format(Locale.ENGLISH,"<ellipse cx=\"%f\" cy=\"%f\" rx=\"%f\" ry=\"%f\" />", center.x,center.y,rx,ry);
//    }
//}


public class Main {

    public static void main(String[] args) {
        Shape poly = new Polygon(new Vec2[]{new Vec2(120,60), new Vec2(270,280), new Vec2(240,320), new Vec2(110,80)});
	poly = new SolidFillShapeDecorator (poly, "purple");
	poly = new StrokeShapeDecorator (poly, "black", 4);
	poly = new DropShadowDecorator (poly);
	poly = new TransformationDecorator.Builder ()
		.setShape(poly)
		.setScale(new Vec2 (2, 0.5))
		.build ();
	//Shape ellipse = new Ellipse (new Vec2(200, 200),50, 100);
	//ellipse = new SolidFillShapeDecorator (ellipse, "yellow");
        SvgScene scene = SvgScene.getInstance ();
        scene.addShape(poly);
	//scene.addShape(ellipse);
        scene.saveHtml("scene.html");

    }
}

class DropShadowDecorator extends ShapeDecorator {

	private static int index = 1;

	public DropShadowDecorator (Shape decoratedShape) {
		super (decoratedShape);
	}

	@Override
	public String toSvg (String parameter) {
		int index = DropShadowDecorator.index++;
		addRequiredDefToScene (index);
		return super.toSvg (String.format ("filter=\"url(#f%d)\" %s ", index, parameter));
	}

	private void addRequiredDefToScene (int index) {
		SvgScene scene = SvgScene.getInstance ();
		scene.addDef (String.format ("\t<filter id=\"f%d\" x=\"-100%%\" y=\"-100%%\" width=\"300%%\" height=\"300%%\">\n" +
"\t\t<feOffset result=\"offOut\" in=\"SourceAlpha\" dx=\"5\" dy=\"5\" />\n" +
"\t\t<feGaussianBlur result=\"blurOut\" in=\"offOut\" stdDeviation=\"5\" />\n" +
"\t\t<feBlend in=\"SourceGraphic\" in2=\"blurOut\" mode=\"normal\" />\n" +
"\t</filter>", index));
	}
}

class TransformationDecorator extends ShapeDecorator {
	private boolean translate = false;
		private Vec2 translateVector;

		private boolean rotate = false;
		private double rotateAngle;
		private Vec2 rotateCenter;

		private boolean scale = false;
		private Vec2 scaleVector;

		private Shape shape;


	static class Builder {
		private boolean translate = false;
		private Vec2 translateVector;

		private boolean rotate = false;
		private double rotateAngle;
		private Vec2 rotateCenter;

		private boolean scale = false;
		private Vec2 scaleVector;

		private Shape shape;


		public Builder setTranslate (Vec2 translateVector) {
			this.translateVector = translateVector;
			translate = true;
			return this;
		}

		public Builder setRotate (double rotateAngle, Vec2 rotateCenter) {
			this.rotateAngle = rotateAngle;
			this.rotateCenter = rotateCenter;
			rotate = true;
			return this;
		}
	
		public Builder setScale (Vec2 scaleVector) {
			this.scaleVector = scaleVector;
			scale = true;
			return this;
		}

		public Builder setShape (Shape shape) {
			this.shape = shape;
			return this;
		}

		public TransformationDecorator build () {
			TransformationDecorator transformationDecorator = new TransformationDecorator (shape);
			transformationDecorator.translate = this.translate;
			if (translate) {
				transformationDecorator.translateVector = this.translateVector;
			}
			transformationDecorator.rotate = this.rotate;
			if (rotate) {
				transformationDecorator.rotateAngle = this.rotateAngle;
				transformationDecorator.rotateCenter = this.rotateCenter;
			}
			transformationDecorator.scale = this.scale;
			if (scale) {
				transformationDecorator.scaleVector = this.scaleVector;
			}
			return transformationDecorator;
		}

	}

	private TransformationDecorator (Shape shape) {
		super (shape);
	}

	@Override
	public String toSvg (String parameter) {
		String result = "";
		if (translate) {
			result += String.format (Locale.ENGLISH, "translate(%f %f) ", translateVector.x, translateVector.y);
		}
		if (rotate) {
			result += String.format (Locale.ENGLISH, "rotate(%f %f %f) ", rotateAngle, rotateCenter.x, rotateCenter.y);
		}
		if (scale) {
			result += String.format (Locale.ENGLISH, "scale(%f %f) ", scaleVector.x, scaleVector.y);
		}

		return super.toSvg (String.format("transform=\"%s\" %s", result, parameter));
	}
}




class ShapeDecorator implements Shape {
	protected Shape decoratedShape;

	public ShapeDecorator (Shape shape) {
		this.decoratedShape = shape;
	}

	public String toSvg (String parameter) {
		return decoratedShape.toSvg (parameter);
	}
}

class SolidFillShapeDecorator extends ShapeDecorator {
	private String color;
	public SolidFillShapeDecorator (Shape shape, String color) {
		super(shape);
		this.color = color;
	}
	
	public String toSvg (String parameter) {
		return super.decoratedShape.toSvg (String.format ("fill=\"%s\" %s", color, parameter));
	}
}

class StrokeShapeDecorator extends ShapeDecorator {
	private String color;
	private double width;

	public StrokeShapeDecorator (Shape decoratedShape, String color, double width) {
		super (decoratedShape);
		this.color = color;
		this.width = width;
	}

	@Override
	public String toSvg (String parameter) {
		return super.toSvg (String.format ("stroke=\"%s\" stroke-width=\"%f\" %s ", color, width, parameter));
	}
}

class Polygon implements Shape {
    Vec2 points[];

    public Polygon(Vec2[] points) {
        this.points = points;
    }

    public String toSvg(String parameter) {
        String pointsString = "";
        for(Vec2 point : points)
            pointsString += point.x + "," + point.y + " ";

        return String.format(Locale.ENGLISH,"<polygon points=\"%s\" %s />", pointsString, parameter);
    }
}


interface Shape {
    String toSvg(String parameter);

}



class SvgScene {
    private Shape shapes[] = new Shape[0];
    private static SvgScene instance = null;
    private String defs[] = new String[0];

    public void addDef (String def) {
	    defs = Arrays.copyOf(defs, defs.length + 1);
	    defs[defs.length - 1] = def;
    }

    public static SvgScene getInstance () {
	    if (instance == null) {
		    instance = new SvgScene ();
	    }
	    return instance;
    }

    private SvgScene () {}

    public void addShape(Shape poly) {
        shapes = Arrays.copyOf(shapes, shapes.length + 1);
        shapes[shapes.length - 1] = poly;
    }

    public void saveHtml(String path) {
        try {
            FileWriter file = new FileWriter(path);
            file.write("<html>\n<body>\n");
	                file.write(String.format("<svg width=1000 height=1000>\n"));
            for(Shape shape : shapes)
                file.write("\t"+ shape.toSvg("")+"\n");
file.write("<defs>\n");
	    for (String def: defs) {
		    file.write (def+"\n");
	    }
	    file.write("</defs>\n");

            file.write("</svg>\n</body>\n</html>\n");
            file.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


class Vec2 {
    public final double x, y;

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
