package com.company;

import java.io.*;
import java.net.Socket;

public class Main {

    public static void main(String[] args){
        ServerThread serverThread = new ServerThread("localhost", 6969);
        serverThread.start();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while(true){
                String message = reader.readLine();
                serverThread.sendToServer(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
