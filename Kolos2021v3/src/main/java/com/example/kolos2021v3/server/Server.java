package com.example.kolos2021v3.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread{
    private ServerSocket serverSocket;
    private List<ClientThread> clients = new ArrayList<>();
    private int port;

    public void setServer(int port){
        this.port = port;
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server now online");
        } catch (IOException e) {
            System.out.println("Server already online");
        }
    }

    @Override
    public void run() {
        Socket clientSocket;
        try {
            while(serverSocket != null){
                clientSocket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(clientSocket,this);
                clients.add(clientThread);
                clientThread.start();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public void broadcast(String message){
        for (ClientThread client:clients) {
            client.send(message);
        }
    }
}
