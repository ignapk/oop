import static java.lang.Math.*;

class Line {
	private Point p, q;
	public Point getP () {
		return p;
	}
	public Point getQ () {
		return q;
	}
	public void setP (Point p) {
		this.p = p;
	}
	public void setQ (Point q) {
		this.q = q;
	}
	public double length () {
		return sqrt (pow (p.x - q.x, 2) + pow (p.y - q.y, 2));
	}
	public Line (Point  p, Point q) {
		this.p = p;
		this.q = q;
	}
}

class Point {
	public final double x, y;
	public Point (double x, double y) {
		this.x = x;
		this.y = y;
	}
}


public class Main {
	public static void main (String[] args) {
		System.out.println ("Hello world!");
		Point p = new Point (1, 1);
		Point q = new Point (0, 0);
		Line l = new Line (p, q);
		System.out.println ("Length: " + l.length());
	}
}
