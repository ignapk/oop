package com.example.kolos2021v3.server;

import com.example.kolos2021v3.Controller;

import java.io.*;
import java.net.Socket;

public class ClientThread extends Thread{
    private Socket clientSocket;
    private Server server;
    private PrintWriter writer;

    public ClientThread(Socket clientSocket, Server server) {
        this.clientSocket = clientSocket;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            writer = new PrintWriter(outputStream,true);    //to true jest w pyte wazne
            String message;

            while((message = reader.readLine())!=null){
                server.broadcast(message); //wowo dziala
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void send(String message){
        writer.println(message);
    }
}
