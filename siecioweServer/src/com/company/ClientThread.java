package com.company;

import java.io.*;
import java.net.Socket;

public class ClientThread extends Thread{
    Socket socket;
    Server server;
    BufferedReader input;
    PrintWriter output;
    public ClientThread(Socket clientSocket, Server server){
        this.socket = clientSocket;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            String message;
            while((message = input.readLine()) != null) {
                //output.println("yay!!");
                System.out.println(message);
                for(ClientThread currentThreadFromList : server.clients){
                    if(currentThreadFromList != this) {
                        currentThreadFromList.send(message);
                    }
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void send(String message){
        output.println(message);
    }
}
