package com.example.kolos2021v3.client;

import com.example.kolos2021v3.Controller;
import com.example.kolos2021v3.server.Server;

import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread{
    private Socket socket;
    private int port;
    private PrintWriter writer;

    private Controller controller;

    public void setController(Controller controller){
        this.controller = controller;
    }

    public void connectToServer(String address,int port){
        try {
            socket = new Socket(address,port);
            System.out.println("Connected to server");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            writer = new PrintWriter(outputStream,true);
            String message;
            while((message = reader.readLine())!=null){
//                if(message.startsWith("C")){
//                    controller.setColor(message.substring(1));
//                }
//                else{
//                    double [] points = new double[4];
//                    String [] pts = message.split(" ");
//                    int i = 0;
//                    for(String p:pts){
//                        if(i==0){
//                            p = p.substring(1);
//                        }
//                        points[i] = Double.parseDouble(p);
//                        i++;
//                    }
//                    controller.drawLine(points[0],points[1],points[2],points[3]);
//                }
                System.out.println(message);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void broadcast(String message){
        writer.println(message);
    }
}
