package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {
    ServerSocket server;
    Socket clientSocket;
    List<ClientThread> clients = new ArrayList<>();

    public Server(int port){
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listen(){
        while(true) {
            try {
                clientSocket = server.accept(); //server zapisuje polaczanie miedzy soba a klientem
                System.out.println("hello");
                ClientThread clientThread = new ClientThread(clientSocket, this);
                clientThread.start();
                clients.add(clientThread);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
