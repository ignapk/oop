package com.example.kolos2021v3.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputThread extends Thread{
    private ServerThread serverThread;

    public InputThread(ServerThread serverThread){
        this.serverThread = serverThread;
    }

    @Override
    public void run() {
        String message;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter values:");
        try {
            while ((message = reader.readLine()) != null) {
                if(message.startsWith("#")){
                    serverThread.broadcast("C"+message);//color
                }
                else{
                    serverThread.broadcast("P"+message);//points
                }
            }
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
