package com.example.kolos2021v3;

import com.example.kolos2021v3.client.InputThread;
import com.example.kolos2021v3.client.ServerThread;
import com.example.kolos2021v3.server.Server;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("App.fxml"));

        Server server = new Server();
        server.setServer(9999);
        server.start();

        ServerThread serverThread = new ServerThread();
        serverThread.connectToServer("localhost",9999);
        serverThread.start();

        InputThread inputThread = new InputThread(serverThread);
        inputThread.start();


        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        stage.setTitle("Lines");
        stage.setScene(scene);
        stage.show();

//        Canvas canvas = new Canvas();
//        Controller controller = new Controller(canvas);
//        serverThread.setController(controller);
//        Scene scene = new Scene(new VBox(canvas),500,500);
//        stage.setTitle("Lines");
//        stage.setScene(scene);
//        stage.setResizable(false);
//        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}