import java.nio.file.Path;

class FileCommander {
	Path path;
	public FileCommander () {
		path = Path.of (System.getProperty ("user.home"));
	}
	public String pwd () {
		return path.toString ();
	}
	public void cd (String dest) {
		path = path.resolve (dest);
	}
}
public class Main {
	public static void main (String[] args) {
		System.out.println ("Hello world!");
		FileCommander path = new FileCommander ();
		System.out.println (path.pwd ());
		path.cd ("ignapk");
		System.out.println (path.pwd ());
	}
}
