package com.company;

import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread {
    Socket socketWithServer;
    PrintWriter output;
    BufferedReader input;
    public ServerThread(String host, int port){
        try {
            this.socketWithServer = new Socket(host, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(socketWithServer.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socketWithServer.getOutputStream()), true);
            String message;
            while((message = input.readLine()) != null){
                System.out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendToServer(String message){
        output.println(message);
    }
}
