import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class Person {
	private String name;
	private LocalDate birth;
	private LocalDate death;

	private static LocalDate parseDate (String string) {
		return LocalDate.parse (string, DateTimeFormatter.ofPattern ("dd.MM.yyyy"));
	}

	public static Person fromFile (String path) {
		try {
			BufferedReader reader = new BufferedReader (new FileReader (path));
			String name = reader.readLine ();
			LocalDate birth = parseDate (reader.readLine ());
			LocalDate death = null;
			try {
				death = parseDate (reader.readLine ()); 
			} catch (NullPointerException exception) {}
			return new Person (name, birth, death);
		} catch (IOException exception) {
			System.out.println ("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		}
		reader.close ();
		return new Person (null, null, null);
	}

	private Person (String name, LocalDate birth, LocalDate death) {
		this.name = name;
		this.birth = birth;
		this.death = death;
	}

	public String toString () {
		return name + " " + birth + " " + death;
	}

}

public class Main {
	public static void main (String[] args) {
		Person person = Person.fromFile ("file");
		System.out.println (person);
	}
}
