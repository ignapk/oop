import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.StringTokenizer;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

class Person {
	private String name;
	private LocalDate birth;
	private LocalDate death;

	private static LocalDate parseDate (String string) {
		return LocalDate.parse (string, DateTimeFormatter.ofPattern ("dd.MM.yyyy"));
	}

	public static Person fromFile (String path) {
		try {
			BufferedReader reader = new BufferedReader (new FileReader (path));
			String name = reader.readLine ();
			LocalDate birth = parseDate (reader.readLine ());
			LocalDate death = null;
			try {
				death = parseDate (reader.readLine ()); 
			} catch (NullPointerException exception) {}
			return new Person (name, birth, death);
		} catch (IOException exception) {
			System.out.println ("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		}
		return new Person (null, null, null);
	}

	public static Person[] fromCsv (String path) {
		List<Person> people = new ArrayList<>();
		try {
			BufferedReader reader = new BufferedReader (new FileReader (path));
			String line;
			while ((line = reader.readLine ()) != null) {
				StringTokenizer token = new StringTokenizer (line, ";");
				String name = token.nextToken ();
				LocalDate birth = parseDate (token.nextToken ());
				LocalDate death = null;
				if (token.hasMoreTokens ()) {
					death = parseDate (token.nextToken ());
				}
				Person person = new Person (name, birth, death);
				people.add (person);
			}
		} catch (IOException e) {
			System.out.println ("AAAAAAAAA");
		}
		Person[] result = people.toArray(new Person[0]);
		return result;
	}

	private Person (String name, LocalDate birth, LocalDate death) {
		this.name = name;
		this.birth = birth;
		this.death = death;
	}

	public String toString () {
		return name + " " + birth + " " + death;
	}

}

public class Main {
	public static void main (String[] args) {
		//Person person = Person.fromFile ("file");
		//System.out.println (person);
		Person[] people = Person.fromCsv ("file.csv");
		for (Person person: people) {
			System.out.println (person);
		}
	}
}
