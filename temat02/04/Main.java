import java.util.Locale;
class Point {
    public final double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
abstract class Shape {
	protected Style style;
	public Shape (Style style) {
		this.style = style;
	}
	public abstract String toSvg ();
}
class Polygon extends Shape {
    Point arr[];

    public Polygon(int count, Style style) {
	super (style);
        arr = new Point[count];
	this.style = style;
    }


    public void setPoint(int index, Point point) {
        arr[index] = point;
    }

    public void setPoints(Point points[]){
        arr = points;
    }

    @Override
    public String toSvg() {
        String pointsString = "";
        for(Point point : arr)
            pointsString += point.x + "," + point.y + " ";

        return String.format(Locale.ENGLISH,"<polygon points=\"%s\" %s/>", pointsString, style.toSvg ());
    }
}
class Style {
	public final String fillColor;
	public final String strokeColor;
	public final Double strokeWidth;
	public Style (String fillColor, String strokeColor, Double strokeWidth) {
		this.fillColor = fillColor;
		this.strokeColor = strokeColor;
		this.strokeWidth = strokeWidth;
	}
	public String toSvg () {
		String fillColor, strokeColor;
		Double strokeWidth;
		fillColor = this.fillColor;
		strokeColor = this.strokeColor;
		strokeWidth = this.strokeWidth;
		if (this.fillColor == null)
			fillColor = "transparent";
		if (this.strokeColor == null)
			strokeColor = "black";
		if (this.strokeWidth == null)
			strokeWidth = 1.d;
		return String.format ("style=\"fill:%s;stroke:%s;stroke-width:%f\"", fillColor, strokeColor, strokeWidth);
	}
}
public class Main {


    public static void main(String[] args) {
        Polygon poly = new Polygon(4, new Style ("pink", "blue", 5.0));
        poly.setPoints(new Point[]{new Point(120,30), new Point(170,180), new Point(240,320), new Point(110,30)});
        System.out.println(poly.toSvg());


    }
}

