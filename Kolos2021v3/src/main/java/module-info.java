module com.example.kolos2021v3 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.kolos2021v3 to javafx.fxml;
    exports com.example.kolos2021v3;
}