import java.io.FileWriter;
import java.util.Arrays;
import java.io.IOException;
import java.util.Locale;
//
//
//
//class Ellipse implements Shape{
//    Vec2 center;
//    double rx,ry;
//
//    public Ellipse(Vec2 center, double rx, double ry) {
//        this.rx=rx;
//        this.ry=ry;
//        this.center=center;
//    }
//
//    @Override
//    public String toSvg() {
//        return String.format(Locale.ENGLISH,"<ellipse cx=\"%f\" cy=\"%f\" rx=\"%f\" ry=\"%f\" />", center.x,center.y,rx,ry);
//    }
//}
class SolidFilledPolygon extends Polygon {
	private String color;
	public SolidFilledPolygon (Vec2[] points, String color) {
		super (points);
		this.color = color;
	}
	public String toSvg (String parameter) {
		return super.toSvg (String.format("fill=\"%s\" %s", color, parameter));
	}
}

public class Main {

    public static void main(String[] args) {
        //Shape poly = new Polygon(new Vec2[]{new Vec2(120,60), new Vec2(270,280), new Vec2(240,320), new Vec2(110,80)});
        Shape poly = new SolidFilledPolygon(new Vec2[]{new Vec2(120,60), new Vec2(270,280), new Vec2(240,320), new Vec2(110,80)}, "pink");

        SvgScene scene=new SvgScene();
        scene.addShape(poly);
        scene.saveHtml("scene.html");

    }
}



class Polygon implements Shape {
    Vec2 points[];

    public Polygon(Vec2[] points) {
        this.points = points;
    }

    public String toSvg(String parameter) {
        String pointsString = "";
        for(Vec2 point : points)
            pointsString += point.x + "," + point.y + " ";

        return String.format(Locale.ENGLISH,"<polygon points=\"%s\" %s />", pointsString, parameter);
    }
}


interface Shape {
    String toSvg(String parameter);

}



class SvgScene {
    private Shape shapes[] = new Shape[0];

    public void addShape(Shape poly) {
        shapes = Arrays.copyOf(shapes, shapes.length + 1);
        shapes[shapes.length - 1] = poly;
    }

    public void saveHtml(String path) {
        try {
            FileWriter file = new FileWriter(path);
            file.write("<html>\n<body>\n");
            file.write(String.format("<svg width=1000 height=1000>\n"));
            for(Shape shape : shapes)
                file.write("\t"+ shape.toSvg("")+"\n");
            file.write("</svg>\n</body>\n</html>\n");
            file.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


class Vec2 {
    public final double x, y;

    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
