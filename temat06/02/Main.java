import java.nio.file.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;

class FileCommander {
	Path path;
	public FileCommander () {
		path = Path.of (System.getProperty ("user.home"));
	}
	public String pwd () {
		return path.toString ();
	}
	public void cd (String dest) {
		path = path.resolve (dest);
	}
	public List<String> ls () {
		/*try {
			Stream<Path> stream = Files.list (path);
			Stream<String> stream2 = stream.map (currentPath -> currentPath.getFileName ().toString ());
			return stream2.collect (Collectors.toList ());
		} catch (IOException e) {
			e.printStackTrace ();
		}
		return new ArrayList<>();*/

		try (Stream<Path> stream = Files.list (path)) {
			Comparator<Path> cmp = (path1, path2) -> Boolean.compare (Files.isDirectory (path2), Files.isDirectory (path1));
			cmp = cmp.thenComparing (currentPath -> currentPath.getFileName ());
		return  stream
			.sorted (cmp)
			.map (currentPath -> {
				//currentPath.getFileName ().toString ()
				if (Files.isDirectory (currentPath))
					return "[" + currentPath.getFileName () + "]"; 
				else
					return currentPath.getFileName ().toString ();
			})
			.collect (Collectors.toList ());
		} catch (IOException e) {
			e.printStackTrace ();
		}
		return new ArrayList<>();
	}
}
public class Main {
	public static void main (String[] args) {
		System.out.println ("Hello world!");
		FileCommander path = new FileCommander ();
		System.out.println (path.pwd ());
		//path.cd ("ignapk");
		//System.out.println (path.pwd ());
		System.out.println (path.ls ());
	}
}
