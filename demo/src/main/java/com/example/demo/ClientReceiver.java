package com.example.demo;

import java.util.List;

public interface ClientReceiver {
    void receiveWord (List<String> words, int wordCount);
}
